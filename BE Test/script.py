#!/usr/bin/python

# python script.py -f example_1.json -s 2 -t "Knife, Potted Plant"
# python script.py -f example_2.json -s 4 -t "Knife, Potted Plant, Pillow"

import sys, getopt
import json
from collections import Counter

data = None
inputfile = None
start = None
targets = None
path = []
directions = ('north', 'east', 'south', 'west')


def getRoom(id):
    for room in data['rooms']:
        if room['id'] == id:
            return room
    return None


def walk(id):
    room = getRoom(id)
    path.append(room)
    if set([stepobject['name'] for step in path for stepobject in step['objects']]) == set(targets):
        # print 'ALL OBJECTS COLLECTED'
        return
    neighbours = [room[direction] for direction in directions if direction in room.keys()]
    neighbours_count = [path.count(getRoom(n)) for n in neighbours]
    # print neighbours
    # print neighbours_count
    next_room = neighbours[neighbours_count.index(min(neighbours_count))]
    # print next_room
    walk(next_room)


def processJson():
    global data
    if inputfile:
        data = json.load(open(inputfile, 'r'))
        walk(start)
        # walk(4)
        # walk(2)
        # walk(1)
        # walk(2)
        # walk(3)
        collected = []
        print 'ID  Room          Object collected'
        print '----------------------------------'
        for step in path:
            items = [stepobject['name'] for stepobject in step['objects']]
            if items not in collected:
                print step['id'], '\t', step['name'], '\t', ''.join(items)
            else:
                print step['id'], '\t', step['name']
            if items:
                collected.append(items)
        

def main(argv):
    global inputfile, start, targets
    try:
        opts, args = getopt.getopt(argv,"hf:s:t:",["inputfile=","start=","targets="])
    except getopt.GetoptError:
        print 'script.py -f <inputfile> -s <start> -t <targets [comma separated]>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'script.py -f <inputfile> -s <start> -t <targets [comma separated]>'
            sys.exit()
        elif opt in ("-f", "--inputfile"):
            inputfile = arg
        elif opt in ("-s", "--start"):
            start = int(arg)
        elif opt in ("-t", "--targets"):
            targets = [a.strip() for a in arg.split(',')]
    # print 'Input file is', inputfile
    # print 'Starting room is', start
    # print 'Objects to collect are', targets

    processJson()


if __name__ == "__main__":
   main(sys.argv[1:])