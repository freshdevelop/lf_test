import React from 'react';
// import { getHeight } from './Utils';
import Input from './Input';

class StepContent3 extends React.Component {

    constructor() {
        super()

        this.state = {
            submitEnabled: false
        }

        this.updateRock = this.updateRock.bind(this)
    }

    updateRock(event) {
        this.setState({
            submitEnabled: event.target.checked
        })
    }

    render() {
        return (
            <>
                <Input
                    type    = "checkbox"
                    id      = "rock"
                    label   = "Are you ready to rock?"
                    onChange = { this.updateRock }
                />
                <Input
                    type    = "submit"
                    id      = "submit_button"
                    value   = "Complete Registration"
                    disabled = { !this.state.submitEnabled }
                    onClick = { this.props.onConfirm }
                />
            </>
        )
    }
}

export default StepContent3;