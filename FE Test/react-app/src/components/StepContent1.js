import React from 'react';
import { getHeight } from './Utils';
import Input from './Input';

class StepContent1 extends React.Component {

    constructor() {
        super()

        this.attendees_max = 5;
        this.attendee_container_heights = [];

        this.state = {
            confirmed: false,
            num_attendees: 0,
            attendee_container_height: 0,
            attendees: [],
        }

        this.updateAttendeesCount = this.updateAttendeesCount.bind(this)
        this.updateAttendee = this.updateAttendee.bind(this)
    }

    updateAttendeesCount(event) {
        let attendees = parseInt(event.target.value, 10);
        this.setState({
            num_attendees: attendees,
            attendee_container_height: this.attendee_container_heights.slice(0, attendees).reduce((a, b) => a + b, 0) + 'px'
        })
    }

    updateAttendee(event, id) {
        let attendees = this.state.attendees;
        attendees[id] = event.target.value;
        let confirmed = (attendees.filter(Boolean).length === this.state.num_attendees);
        this.setState({
            confirmed: confirmed,
            attendees: attendees,
            attendee_container_height: (confirmed) ? '1000px' : this.state.attendee_container_height
        })
        if (confirmed) {
            this.props.onConfirm()
        }
    }

    componentDidMount() {
        for (let i = 1; i <= this.attendees_max; i++) {
            this.attendee_container_heights.push(getHeight(document.getElementById('attendee_' + i + '_wrap')));
        }
    }

    render() {
        return (
            <>
            <Input 
                type    = "select"
                id      = "num_attendees"
                label   = "How many people will be attending?"
                options = {[
                    { id: 0, label: "Please Choose" },
                    { id: 1 },
                    { id: 2 },
                    { id: 3 },
                    { id: 4 },
                    { id: 5 },
                ]}
                onChange = { this.updateAttendeesCount }
                value = { this.state.num_attendees }
            />
            <br />
            <div id="attendee_container" style={{ maxHeight: this.state.attendee_container_height }}>
                { (!this.state.confirmed || (this.state.confirmed && this.state.num_attendees >= 1)) &&
                    <div id="attendee_1_wrap">
                        <h3>Please provide full names:</h3>
                        <Input
                            type    = "text"
                            id      = "name_attendee_1"
                            label   = "Attendee 1 Name:"
                            onChange = { (event) => { this.updateAttendee(event, 0) } }
                            value   = { this.state.attendees[0] }
                        />
                    </div>
                }
                { (!this.state.confirmed || (this.state.confirmed && this.state.num_attendees >= 2)) &&
                    <div id="attendee_2_wrap">
                        <Input
                            type    = "text"
                            id      = "name_attendee_2"
                            label   = "Attendee 2 Name:"
                            onChange = { (event) => { this.updateAttendee(event, 1) } }
                            value   = { this.state.attendees[1] }
                        />
                    </div>
                }
                { (!this.state.confirmed || (this.state.confirmed && this.state.num_attendees >= 3)) &&
                    <div id="attendee_3_wrap">
                        <Input
                            type    = "text"
                            id      = "name_attendee_3"
                            label   = "Attendee 3 Name:"
                            onChange = { (event) => { this.updateAttendee(event, 2) } }
                            value   = { this.state.attendees[2] }
                        />
                    </div>
                }
                { (!this.state.confirmed || (this.state.confirmed && this.state.num_attendees >= 4)) &&
                    <div id="attendee_4_wrap">
                        <Input
                            type    = "text"
                            id      = "name_attendee_4"
                            label   = "Attendee 4 Name:"
                            onChange = { (event) => { this.updateAttendee(event, 3) } }
                            value   = { this.state.attendees[3] }
                        />
                    </div>
                }
                { (!this.state.confirmed || (this.state.confirmed && this.state.num_attendees >= 5)) &&
                    <div id="attendee_5_wrap">
                        <Input
                            type    = "text"
                            id      = "name_attendee_5"
                            label   = "Attendee 5 Name:"
                            onChange = { (event) => { this.updateAttendee(event, 4) } }
                            value   = { this.state.attendees[4] }
                        />
                    </div>
                }
                <div id="step1_result"></div>
            </div>
            </>
        )
    }
}

export default StepContent1;