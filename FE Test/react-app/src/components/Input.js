import React from 'react';

class Input extends React.Component {

    renderLabel() {
        if (this.props.label) {
            return (
                <label htmlFor={ this.props.id }>
                    { this.props.label }
                </label>
            )
        }
        return;
    }
    
    render() {
        return (
            <>
                { !this.props.renderLabel || this.props.renderLabel === 'before' ? this.renderLabel() : '' }

                {(() => {
                    switch (this.props.type) {

                        case 'select':
                            return (
                                <select id={ this.props.id } value={ this.props.value } onChange={ this.props.onChange }>
                                    { this.props.options.map(
                                        option => <option key={ option.id } id={ "opt_" + option.id } value={ option.id }>{ option.label ? option.label : option.id }</option>
                                    ) }
                                </select>
                            )

                        case 'radio':
                            return (
                                <input type="radio" id={ this.props.id } name={ this.props.name } onChange={ this.props.onChange } />
                            )

                        case 'checkbox':
                            return (
                                <input type="checkbox" id={ this.props.id } onChange={ this.props.onChange } />
                            )

                        case 'submit':
                            return (
                                <input type="submit" id={ this.props.id } value={ this.props.value } onClick={ this.props.onClick } disabled={ this.props.disabled ? 'disabled' : '' } />
                            )

                        case 'text': default:
                            if (this.props.onChange) {
                                return (
                                    <input type="text" id={ this.props.id } value={ this.props.value || '' } onChange={ this.props.onChange } />
                                )
                            } else {
                                return (
                                    <input type="text" id={ this.props.id } defaultValue={ this.props.value || '' } />
                                )
                            }
                    }
                })()}

                { this.props.renderLabel === 'after' ? this.renderLabel() : '' }
            </>
        )
    }

}

export default Input;