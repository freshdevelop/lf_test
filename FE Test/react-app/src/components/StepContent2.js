import React from 'react';
import { getHeight } from './Utils';
import Input from './Input';

class StepContent2 extends React.Component {

    constructor() {
        super()

        this.state = {
            badge_container_height: null,
            accommodations_container_height: null,
            step_result_height: 0,
            badge_valid: false,
            accommodations_valid: false
        }

        this.updateBadge = this.updateBadge.bind(this)
    }

    updateBadge(event, value) {
        let confirmed = this.state.accommodations_valid;
        this.setState({
            badge_container_height: value ? this.badge_container_maxheight : 0,
            badge_valid: true,
            step_result_height: confirmed ? '1000px' : 0
        })
        if (confirmed) {
            this.props.onConfirm()
        }
    }

    updateAccommodations(event, value) {
        let confirmed = this.state.badge_valid;
        this.setState({
            accommodations_container_height: value ? this.accommodations_container_maxheight : 0,
            accommodations_valid: true,
            step_result_height: confirmed ? '1000px' : 0
        })
        if (confirmed) {
            this.props.onConfirm()
        }
    }

    componentDidMount() {
        this.badge_container_maxheight = getHeight(document.getElementById('company_name_wrap'))
        this.accommodations_container_maxheight = getHeight(document.getElementById('special_accommodations_wrap'))
        this.setState({
            badge_container_height: 0,
            accommodations_container_height: 0
        })
    }

    render() {
        return (
            <>
                <p>
                    Would you like your company name on your badges?
                </p>
                <Input
                    type    = "radio"
                    id      = "company_name_toggle_on"
                    name    = "company_name_toggle_group"
                    label   = "Yes"
                    label_position = "after"
                    onChange = { (event) => { this.updateBadge(event, 1) } }
                />
                &emsp;
                <Input
                    type    = "radio"
                    id      = "company_name_toggle_off"
                    name    = "company_name_toggle_group"
                    label   = "No"
                    label_position = "after"
                    onChange = { (event) => { this.updateBadge(event, 0) } }
                />
                <div id="company_name_wrap" style={{ maxHeight: this.state.badge_container_height }}>
                    <Input
                        type    = "text"
                        id      = "company_name"
                        label   = "Company Name:"
                    />
                </div>
                <div>			
                    <p>
                        Will anyone in your group require special accommodations?
                    </p>
                    <Input
                        type    = "radio"
                        id      = "special_accommodations_toggle_on"
                        name    = "special_accommodations_toggle"
                        label   = "Yes"
                        label_position = "after"
                        onChange = { (event) => { this.updateAccommodations(event, 1) } }
                    />
                    &emsp;
                    <Input
                        type    = "radio"
                        id      = "special_accommodations_toggle_off"
                        name    = "special_accommodations_toggle"
                        label   = "No"
                        label_position = "after"
                        onChange = { (event) => { this.updateAccommodations(event, 0) } }
                    />
                </div>
                <div id="special_accommodations_wrap" style={{ maxHeight: this.state.accommodations_container_height }}>
                    <label htmlFor="special_accomodations_text">
                        Please explain below:
                    </label>
                    <textarea rows="10" cols="10" id="special_accomodations_text"></textarea>
                </div>
                <div id="step2_result" style={{ maxHeight: this.state.step_result_height }}></div>
            </>
        )
    }
}

export default StepContent2;