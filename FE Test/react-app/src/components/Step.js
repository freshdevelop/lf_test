import React from 'react';
import StepContent1 from './StepContent1';
import StepContent2 from './StepContent2';
import StepContent3 from './StepContent3';

class Step extends React.Component {

    render() {
        return (

            <fieldset id={ 'step_' + this.props.id } disabled={ this.props.disabled ? 'disabled' : '' }>
                <legend>Step { this.props.id }</legend>
                {(() => {
                    switch (this.props.id) {
                        case '1':
                            return <StepContent1 onConfirm={ this.props.onConfirm } />
                        case '2':
                            return <StepContent2 onConfirm={ this.props.onConfirm } />
                        case '3':
                            return <StepContent3 onConfirm={ this.props.onConfirm } />
                        default:
                            return;
                    }
                })()}
            </fieldset>

        )
    }
}

export default Step;