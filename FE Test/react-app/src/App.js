import React from 'react';
import Step from './components/Step';
import './App.scss';

class App extends React.Component {

    constructor() {
        super()

        this.state = {
            step_1_enabled: true,
            step_2_enabled: false,
            step_3_enabled: false
        }

        this.onConfirm = this.onConfirm.bind(this)
    }

    onConfirm(id) {
        switch(id) {
            case 1:
                this.setState({ 
                    step_2_enabled: true
                })
            break;
            case 2:
                this.setState({ 
                    step_3_enabled: true
                })
            break;
            case 3:
                this.setState({ 
                    step_2_enabled: false,
                    step_3_enabled: false,
                })
            break;
            default:
                return
        }
    }

    render() {
        return (
            <div id="page-wrap">
            <h1>Seminar <span>Registration</span></h1>
            <form action="#" method="post">
                <Step id="1" disabled={ !this.state.step_1_enabled } onConfirm={ () => this.onConfirm(1) } />
                <Step id="2" disabled={ !this.state.step_2_enabled } onConfirm={ () => this.onConfirm(2) } />
                <Step id="3" disabled={ !this.state.step_3_enabled } onConfirm={ () => this.onConfirm(3) } />
            </form>
        </div>
    )}
}

export default App;
