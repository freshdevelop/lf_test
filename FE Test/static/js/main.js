ready(function() {
    init();
    initStep1();
    initStep2();
    initStep3();
})

function init() {
    document.getElementById('step_2').disabled = document.getElementById('step_3').disabled = true;
}

function initStep1() {
    var attendees_max = 5;
    var attendee_container = document.getElementById('attendee_container');
    var heights = [];
    var values = [];
    var values_max = 0;

    for (var i = 1; i <= attendees_max; i++) {
        heights.push(getHeight(document.getElementById('attendee_' + i + '_wrap')));

        document.getElementById('name_attendee_' + i).setAttribute('data-id', i);
        addEventHandler(document.getElementById('name_attendee_' + i), 'input', function() {
            values[parseInt(this.getAttribute('data-id'), 10) - 1] = this.value;
            if (values.filter(Boolean).length == values_max) {
                for (var i = values_max + 1; i <= attendees_max; i++) {
                    document.getElementById('attendee_' + i + '_wrap').style.display = 'none';
                }
                attendee_container.style.maxHeight = '1000px';
                enableStep2();
            }
        });
    }

    attendee_container.setAttribute('data-max-height', getHeight(attendee_container) + 'px');
    attendee_container.style.maxHeight = '0';
    addEventHandler(document.getElementById('num_attendees'), 'change', function() {
        attendee_container.style.maxHeight = heights.slice(0, this.value).reduce((a, b) => a + b, 0) + 'px';
        values_max = parseInt(this.value, 10);
    });
}

function initStep2() {
    var company_valid = accommodations_valid = false;
    var company_container = document.getElementById('company_name_wrap');
    company_container.setAttribute('data-max-height', getHeight(company_container) + 'px');
    company_container.style.maxHeight = '0';
    addEventHandler(document.getElementById('company_name_toggle_on'), 'change', function() {
        company_container.style.maxHeight = company_container.getAttribute('data-max-height');
        company_valid = true;
        if (company_valid && accommodations_valid) { confirmStep2(); }
    });
    addEventHandler(document.getElementById('company_name_toggle_off'), 'change', function() {
        company_container.style.maxHeight = '0';
        company_valid = true;
        if (company_valid && accommodations_valid) { confirmStep2(); }
    });

    var accommodations_container = document.getElementById('special_accommodations_wrap');
    accommodations_container.setAttribute('data-max-height', getHeight(accommodations_container) + 'px');
    accommodations_container.style.maxHeight = '0';
    addEventHandler(document.getElementById('special_accommodations_toggle_on'), 'change', function() {
        accommodations_container.style.maxHeight = accommodations_container.getAttribute('data-max-height');
        accommodations_valid = true;
        if (company_valid && accommodations_valid) { confirmStep2(); }
    });
    addEventHandler(document.getElementById('special_accommodations_toggle_off'), 'change', function() {
        accommodations_container.style.maxHeight = '0';
        accommodations_valid = true;
        if (company_valid && accommodations_valid) { confirmStep2(); }
    });

    var step2_result = document.getElementById('step2_result');
    step2_result.setAttribute('data-max-height', getHeight(step2_result) + 'px');
    step2_result.style.maxHeight = '0';
}

function confirmStep2() {
    var step2_result = document.getElementById('step2_result');
    step2_result.style.maxHeight = step2_result.getAttribute('data-max-height');
    enableStep3();
}

function initStep3() {
    var submit_button = document.getElementById('submit_button');
    submit_button.disabled = true;
    addEventHandler(document.getElementById('rock'), 'change', function() {
        submit_button.disabled = !this.checked;
    });
    addEventHandler(document.getElementById('submit_button'), 'change', reset);
}

function enableStep2() {
    document.getElementById('step_2').disabled = false;
}

function enableStep3() {
    document.getElementById('step_3').disabled = false;
}